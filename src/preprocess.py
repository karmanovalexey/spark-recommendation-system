
import os
import configparser
from logger import Logger

SHOW_LOG = True

class Preprocessor():

    def __init__(self):
        self.config = configparser.ConfigParser()
        self.log = Logger(SHOW_LOG).get_logger(__name__)
        self.config_path = os.path.join(os.getcwd(), 'config.ini')
        self.config.read(self.config_path)
        self.log.info(f'CWD: {os.getcwd()}')

    def get_data(self):
        path = self.config.get("DATA", "FILE_PATH")
        if not path:
            self.log.error('Data file path incorrect')
            raise Exception('Data file path incorrect')

        if os.path.exists(path):
            self.log.info(f'Data file found in {path}')
            return True
        else:
            self.log.info(f'Data file not found in {path}')
            return False

if __name__ == "__main__":
    preproc = Preprocessor()
    preproc.get_data()
