import os
import traceback
import configparser

from pyspark.ml.feature import HashingTF, IDF, IDFModel
from pyspark.mllib.linalg import Vectors
from pyspark.mllib.linalg.distributed import IndexedRow, IndexedRowMatrix
from pyspark.mllib.linalg.distributed import MatrixEntry, CoordinateMatrix

from utils import rm_dir, SparkAdapter
from logger import Logger

SHOW_LOG = True

class Inference():
    def __init__(self):
        """
        Initialize Inference
        """

        self.config = configparser.ConfigParser()
        self.log = Logger(SHOW_LOG).get_logger(__name__)
        self.config_path = os.path.join(os.getcwd(), 'config.ini')
        self.config.read(self.config_path)
        try:
            self.adapter = SparkAdapter()
            self.sc = self.adapter.get_context()
            self.spark = self.adapter.get_session()
        except:
            self.log.error(traceback.format_exc())

        mat_ready = self.load_matrix()
        tf_ready = self.load_tf()
        idf_ready = self.load_idf()
        feat_ready = self.load_idf_features()
        tfidf_loaded = mat_ready and tf_ready and idf_ready and feat_ready
        self.log.info(f'TfIdfLoaded: {tfidf_loaded}')

    def load_matrix(self):
        path = self.config.get("FEATURES", "WATCHED_PATH")
        try:
            self.watched = CoordinateMatrix(self.spark.read.parquet(path) \
                .rdd.map(lambda row: MatrixEntry(*row)))
        except:
            self.log.error(traceback.format_exc())
            return False
        return True
    
    def load_tf(self):
        path = self.config.get("FEATURES", "TF_PATH")
        try:
            self.hashingTF = HashingTF.load(path)
        except:
            self.log.error(traceback.format_exc())
            return False
        return True
    
    def load_idf(self):
        path = self.config.get("FEATURES", "IDF_PATH")
        try:
            self.idf = IDFModel.load(path)
        except:
            self.log.error(traceback.format_exc())
            return False
        return True
    
    def load_idf_features(self):
        path = self.config.get("FEATURES", "IDF_FEATURES_PATH")
        try:
            self.idf_features = self.spark.read.load(path)
        except:
            self.log.error(traceback.format_exc())
            return False
        return True
    
    def recommend(self, ordered_similarity, max_count=5):
        """
        args: ordered_similarity: IndexedRow inversely sorted
        out: [(movie, rank),...]
        """
        self.log.info('Calculate movies ranks')
        users_sim_matrix = IndexedRowMatrix(ordered_similarity)
        multpl = users_sim_matrix.toBlockMatrix().transpose().multiply(self.watched.toBlockMatrix())
        ranked_movies = multpl.transpose().toIndexedRowMatrix().rows.sortBy(lambda row: row.vector.values[0], ascending=False)
        result = []
        for i, row in enumerate(ranked_movies.collect()):
            if i >= max_count:
                break
            result.append((row.index, row.vector[0]))
        return result

    def infer(self):
        """
        Infer USER_NUM recommendations
        """

        self.log.info('Inferrence started')

        temp_matrix = IndexedRowMatrix(self.idf_features.rdd.map(
            lambda row: IndexedRow(row["user"], Vectors.dense(row["idf"]))
        ))
        temp_block = temp_matrix.toBlockMatrix()

        similarities = temp_block.transpose().toIndexedRowMatrix().columnSimilarities()
        user_id = self.config.getint("INFERENCE", "USER_NUM")

        filtered = similarities.entries.filter(lambda x: x.i == user_id or x.j == user_id)

        ordered_similarity = filtered.sortBy(lambda x: x.value, ascending=False) \
            .map(lambda x: IndexedRow(x.j if x.i == user_id else x.i, Vectors.dense(x.value)))

        recomendations = self.recommend(ordered_similarity, max_count=self.config.getint("INFERENCE", "MAX_COUNT"))
        self.log.info('Recommendations:')
        for movie_id, rank in recomendations:
            self.log.info(f'Movie #{movie_id} (rank: {rank})')

if __name__ == "__main__":
    processor = Inference()
    processor.infer()