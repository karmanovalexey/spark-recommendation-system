
import sys
import os

sys.path.insert(1, os.path.join(os.getcwd(), "src"))
from train import Trainer

trainer = Trainer()

def test_train_models():
    assert trainer.train()
