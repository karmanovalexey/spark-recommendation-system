import os
import shutil
import traceback
import configparser

from pyspark.ml.feature import HashingTF, IDF
from pyspark.mllib.linalg import Vectors
from pyspark.mllib.linalg.distributed import IndexedRow, IndexedRowMatrix
from pyspark.mllib.linalg.distributed import MatrixEntry, CoordinateMatrix

from utils import SparkAdapter

from logger import Logger
from utils import rm_dir, SparkAdapter

SHOW_LOG = True



class Trainer():

    def __init__(self):
        """
        Initialize config, logger
        """
        self.config = configparser.ConfigParser()
        self.log = Logger(SHOW_LOG).get_logger(__name__)
        self.config_path = os.path.join(os.getcwd(), 'config.ini')
        self.config.read(self.config_path)
        self.log.info("Trainer Initialized")

        self.tf = None
        self.idf = None
    
    def calculate_tf(self, data, path='./cache/tf'):
        """
        Подсчет Term Frequency
        """
        self.log.info('Calculating TF')
        num_features = self.config.getint("FEATURES", "NUM_FEATURES")
        rm_dir(path)

        df = data.toDF(schema=["user", "movie"])


        hashingTF = HashingTF(inputCol="movie", outputCol="tf", numFeatures=num_features)
        self.tf = hashingTF.transform(df)

        try:
            hashingTF.write().overwrite().save(path)
            self.config["FEATURES"]["TF_PATH"] = path
            self.log.info(f"TF saved at {path}")
        except:
            self.log.error(traceback.format_exc())
            return False
        
        return True
    
    def calculate_idf(self, idf_doc='./cache/idf', idf_out='./cache/idf_out'):
        """
        Подсчет Inverse Document Frequency
        """
        self.log.info('Caluclating IDF')
        rm_dir(idf_doc)
        if self.tf != None:
            tf = self.tf
        else:
            return False

        idf_model = IDF(inputCol="tf", outputCol="idf")
        self.idf = idf_model.fit(tf)

        try:
            self.idf.write().overwrite().save(idf_doc)
            self.config["FEATURES"]["IDF_PATH"] = idf_doc
            self.log.info(f"IDF saved at {idf_doc}")
        except:
            self.log.error(traceback.format_exc())
            return False

        idf_features = self.idf.transform(tf)        
        rm_dir(idf_out)

        try:
            idf_features.write.format("parquet").save(idf_out, mode='overwrite')
            self.config["FEATURES"]["IDF_FEATURES_PATH"] = idf_out
            self.log.info(f"IDF features stored at {idf_out}")
        except:
            self.log.error(traceback.format_exc())
            return False

        return True

    def to_matrix(self, data, path='./cache/matrix'):
        """
        Raw data to matrix
        """
        rm_dir(path)

        matrix = CoordinateMatrix(data.flatMapValues(lambda x: x).map(lambda x: MatrixEntry(x[0], x[1], 1.0)))
        
        try:
            matrix.entries.toDF().write.parquet(path)
            self.config["FEATURES"]["WATCHED_PATH"] = path
            self.log.info(f"Matrix saved at {path}")
        except:
            self.log.error(traceback.format_exc())
            return False
        return os.path.exists(path)

    def train(self):
        try:
            adapter = SparkAdapter()
            context = adapter.get_context()
            session = adapter.get_session()
        except:
            self.log.error(traceback.format_exc())
            return False

        data_path = self.config.get("DATA", "FILE_PATH", fallback="./data/sample.csv")
        self.log.info(f'Processing {data_path}')

        # Read and Load file with multiple films per viewer
        data = context.textFile(data_path, adapter.num_parts) \
            .map(lambda x: map(int, x.split(','))).groupByKey() \
            .map(lambda x : (x[0], list(x[1])))
        
        # Mapped to Matrix
        self.log.info('Data To Matrix')
        self.to_matrix(data)
        
        self.calculate_tf(data)
        self.calculate_idf()

        os.remove(self.config_path)
        with open(self.config_path, 'w') as configfile:
            self.config.write(configfile)

        if self.tf is None or self.idf is None:
            return False
        else:
            return True

if __name__ == "__main__":
    trainer = Trainer()
    trainer.train()